package hr.unios.ferit.stark2;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import java.io.IOException;

public class ActivityToalet extends Activity {

    ImageButton pranjeRukuTipka;
    ImageButton kupanjeTipka;
    ImageButton pranjeZubiTipka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_toalet);

        pranjeRukuTipka = (ImageButton) findViewById(R.id.ibPranjeRuku);
        pranjeRukuTipka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                zvukPranjaRuku();
                Intent intentPranjeRuku = new Intent(ActivityToalet.this, ActivityPranjeRukuGIF.class);
                startActivity(intentPranjeRuku);
            }
        });

        kupanjeTipka = (ImageButton) findViewById(R.id.ibKupanje);
        kupanjeTipka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                zvukKupanja();
                Intent intentKupanje = new Intent(ActivityToalet.this, ActivityKupanjeGIF.class);
                startActivity(intentKupanje);
            }
        });

        pranjeZubiTipka = (ImageButton) findViewById(R.id.ibPranjeZubi);
        pranjeZubiTipka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                zvukPranjaZubi();
                Intent intentPranjeZubi = new Intent(ActivityToalet.this, ActivityPranjeZubiGIF.class);
                startActivity(intentPranjeZubi);
            }
        });
    }


    public void zvukPranjaRuku() {
        MediaPlayer mp = MediaPlayer.create(ActivityToalet.this, R.raw.zvuk_pranja_ruku);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.release();
            }

        });
        mp.start();
    }


    public void zvukKupanja() {
        MediaPlayer mp = MediaPlayer.create(ActivityToalet.this, R.raw.zvuk_kupanje);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.release();
            }

        });
        mp.start();
    }

    public void zvukPranjaZubi() {
        MediaPlayer mp = MediaPlayer.create(ActivityToalet.this, R.raw.zvuk_pranja_zubi);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.release();
            }

        });
        mp.start();
    }
}