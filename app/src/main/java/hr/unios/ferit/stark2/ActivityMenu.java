package hr.unios.ferit.stark2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ActivityMenu extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_menu);
    }

    public void idiNaIgraliste(View v) {
        Intent Intentjedan = new Intent(getBaseContext(), ActivityPlayground.class);
        startActivity(Intentjedan);
    }

    public void idiUToalet(View v) {
        Intent intentdva = new Intent(getBaseContext(), ActivityToalet.class);
        startActivity(intentdva);
    }

    public void idiUSobu(View v) {
        Intent intenttri = new Intent(getBaseContext(), ActivitySoba.class);
        startActivity(intenttri);
    }

    public void idiUKuhinju(View v) {
        Intent intentcetiri = new Intent(getBaseContext(), ActivityKuhinja.class);
        startActivity(intentcetiri);
    }
}
