package hr.unios.ferit.stark2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ActivityMain extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_main);
    }

    public void idiNaIzbornik(View v) {
        Intent intent = new Intent(getBaseContext(), ActivityMenu.class);
        startActivity(intent);
    }
}
