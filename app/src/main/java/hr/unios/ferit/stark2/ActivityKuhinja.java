package hr.unios.ferit.stark2;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import java.io.IOException;

public class ActivityKuhinja extends Activity {

    ImageButton hranjenjeTipka;
    ImageButton pranjeSudaTipka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_kuhinja);

        hranjenjeTipka = (ImageButton) findViewById(R.id.ibHranjenje);
        hranjenjeTipka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hranjenje();
                Intent intentHranjenje = new Intent(ActivityKuhinja.this, ActivityHranjenjeGIF.class);
                startActivity(intentHranjenje);
            }
        });

        pranjeSudaTipka = (ImageButton) findViewById(R.id.ibPranjeSuda);
        pranjeSudaTipka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pranjeSuda();
                Intent intentPranjeSuda = new Intent(ActivityKuhinja.this, ActivityPranjeSudaGIF.class);
                startActivity(intentPranjeSuda);
            }
        });
    }

    public void hranjenje() {
        MediaPlayer mp = MediaPlayer.create(ActivityKuhinja.this, R.raw.zvuk_hranjenja);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.release();
            }

        });
        mp.start();
    }

    public void pranjeSuda() {
        MediaPlayer mp = MediaPlayer.create(ActivityKuhinja.this, R.raw.zvuk_pranja_suda);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.release();
            }

        });
        mp.start();
    }
}
