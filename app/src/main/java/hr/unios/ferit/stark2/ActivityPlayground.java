package hr.unios.ferit.stark2;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;


public class ActivityPlayground extends Activity {

    ImageButton biciklTipka;
    ImageButton ljuljackaTipka;
    ImageButton klackalicaTipka;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_playground);

        biciklTipka = (ImageButton) findViewById(R.id.ibBicikl);
        biciklTipka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                zvukBicikla();
                Intent intentBicikl = new Intent(ActivityPlayground.this, ActivityVoznjaBiciklaGIF.class);
                startActivity(intentBicikl);
            }
        });

        ljuljackaTipka = (ImageButton) findViewById(R.id.ibLjuljanje);
        ljuljackaTipka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                zvukLjuljanja();
                Intent intentLjuljacka = new Intent(ActivityPlayground.this, ActivityLjuljanjeGIF.class);
                startActivity(intentLjuljacka);
            }
        });

        klackalicaTipka = (ImageButton) findViewById(R.id.ibKlackanje);
        klackalicaTipka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                zvukKlackanja();
                Intent intentKlackalica = new Intent(ActivityPlayground.this, ActivityKlackanjeGIF.class);
                startActivity(intentKlackalica);
            }
        });
    }

    public void zvukBicikla() {
        MediaPlayer mp = MediaPlayer.create(ActivityPlayground.this, R.raw.zvuk_bicikl);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.release();
            }

        });
        mp.start();
    }

    public void zvukLjuljanja() {
        MediaPlayer mp = MediaPlayer.create(ActivityPlayground.this, R.raw.zvuk_ljuljanje);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.release();
            }

        });
        mp.start();
    }

    public void zvukKlackanja() {
        MediaPlayer mp = MediaPlayer.create(ActivityPlayground.this, R.raw.zvuk_klackanje);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.release();
            }

        });
        mp.start();
    }





}
