package hr.unios.ferit.stark2;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import java.io.IOException;

public class ActivitySoba extends Activity {

    ImageButton citanjeKnjigeTipka;
    ImageButton slaganjeOdjeceTipka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_soba);

        citanjeKnjigeTipka = (ImageButton) findViewById(R.id.ibCitanjeKnjige);
        citanjeKnjigeTipka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                citanjeKnjige();
                Intent intentCitanjeKnjige = new Intent(ActivitySoba.this, ActivityCitanjeKnjigeGIF.class);
                startActivity(intentCitanjeKnjige);
            }
        });

        slaganjeOdjeceTipka = (ImageButton) findViewById(R.id.ibSlaganjeOdjece);
        slaganjeOdjeceTipka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slaganjeOdjece();
                Intent intentSlaganjeOdjece = new Intent(ActivitySoba.this, ActivitySlaganjeOdjeceGIF.class);
                startActivity(intentSlaganjeOdjece);
            }
        });
    }

    public void citanjeKnjige() {
        MediaPlayer mp = MediaPlayer.create(ActivitySoba.this, R.raw.zvuk_citanja_knjige);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.release();
            }

        });
        mp.start();
    }

    public void slaganjeOdjece() {
        MediaPlayer mp = MediaPlayer.create(ActivitySoba.this, R.raw.zvuk_slaganja_odjece);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.release();
            }

        });
        mp.start();
    }
}